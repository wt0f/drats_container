FROM debian:buster-slim
MAINTAINER "Gerard Hickey <wt0f@arrl.net>"

ARG VERSION=0.3.9

WORKDIR /usr/src
RUN apt-get update -y && \
    apt-get install -y python-glade2 python-libxml2 python-libxslt1 python-serial git
RUN git clone https://github.com/maurizioandreotti/D-Rats.git && \
    cd D-Rats && git checkout $VERSION && sed '/lzhuf/d' setup.py > setup2.py && mv setup2.py setup.py
RUN cd D-Rats && python setup.py build && python setup.py install

FROM debian:buster-slim

LABEL maintainer="Gerard Hickey <wt0f@arrl.net>"
LABEL version=$VERSION
LABEL volume="/home/drats"
LABEL ports="9000"

RUN apt-get update -y && \
    apt-get install -y python-serial python-six && \
    groupadd drats && useradd -g drats -d /home/drats -m -c 'D-RATS user' drats

EXPOSE 9000

USER drats:drats
WORKDIR /home/drats
COPY --chown=drats:drats repeater.config .
COPY --from=0 /usr/local/bin/d-rats_repeater.py /usr/local/bin/d-rats_repeater
COPY --from=0 /usr/local/lib/python2.7/dist-packages /usr/local/lib/python2.7/dist-packages


CMD ["d-rats_repeater", "--console", "--debug", "--config", "."]
