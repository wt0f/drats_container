# drats_container

D-RATS Ratflector for the D-STAR network.

## Usage

The only thing that needs to be supplied is a `repeater.config` file. This
file is best constructed by starting with the [default settings](#default-repeater.config)
and making the appropriate changes for the instance.

Once the configuration file is completed, the D-RATS repeater can be started
with the following command:

```
docker run -d -p 9000:9000 -v $PWD/repeater.config:/home/drats/repeater.config d-rats_repeater
```

The log output will be sent to STDOUT.

### Default repeater.config

```
[settings]
# devices = [('net:ref035.dstargateway.org:9000', '')]
devices = []
acceptnet = True
netport = 9000
id = NOCALL
idfreq = 30
require_auth = False
trust_local = True
gpsport = 9500
state = True

[tweaks]
allow_gps =
```

### D-RATS source code repository

https://github.com/maurizioandreotti/D-Rats
